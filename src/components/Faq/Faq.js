import React from 'react';
import shortid from 'shortid';
import Container from '../Container/Container';
import Hero from '../Hero/Hero';
import {faqContent, faqQuestions} from '../../data/dataStore';

const Faq = () => (
  <Container>
    <Hero titleText={faqContent.title} image={faqContent.image}/>
    {faqQuestions.map(question =>
      <div key={shortid.generate()}><h2>{question.subtitle}</h2><p>{question.description}</p></div>
    )}
  </Container>
);

export default Faq;