import React from 'react';
import styles from './List.scss';
import Hero from '../Hero/Hero';
import Column from '../Column/ColumnContainer';
import Creator from '../Creator/Creator';
import Container from '../Container/Container';
import PropTypes from 'prop-types';
import {settings} from '../../data/dataStore';
import ReactHtmlParser from 'react-html-parser';
import {DragDropContext} from 'react-beautiful-dnd';

class List extends React.Component {

    static propTypes = {
      title: PropTypes.string.isRequired,
      image: PropTypes.string,
      description: PropTypes.node,
      columns: PropTypes.array,
      addColumn: PropTypes.func,
      moveCard: PropTypes.func,
    }

    static defaultProps = {
      description: settings.defaultListDescription,
      image: settings.defaultListImage,
    }

    render() {
      const {title, image, description, columns, addColumn, moveCard} = this.props;
      const moveCardHandler = result => {
        if(result.destination && (result.destination.index !== result.source.index || result.destination.droppableId !== result.source.droppableId)) {
          moveCard({
            id: result.draggableId,
            dest: {
              index: result.destination.index,
              columnId: result.destination.droppableId,
            },
            src: {
              index: result.source.index,
              columnId: result.source.droppableId,
            },
          });
        }
      };
      return (
        <Container>
          <section className={styles.component}>
            <Hero titleText={title} image={image} />
            <div className={styles.description}>
              {ReactHtmlParser(description)}
            </div>
            <DragDropContext onDragEnd={moveCardHandler}>
              <div className={styles.columns}>
                {columns.map(columnData => (
                  <Column key={columnData.id} {...columnData} />
                ))}
              </div>
            </DragDropContext>
            <div className={styles.creator}>
              <Creator text={settings.columnCreatorText} action={title => addColumn(title)}/>
            </div>
          </section>
        </Container>
      );
    }
}

export default List;
