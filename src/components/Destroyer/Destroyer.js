import React from 'react';
import Button from '../Button/Button';
import PropTypes from 'prop-types';

class Destroyer extends React.Component {
  static propTypes = {
    action: PropTypes.func,
  }

  handleRemove = () => {
    this.props.action(this.state);
  }

  render() {
    return (
      <div>
        <Button onClick={this.handleRemove} variant='danger'>Remove</Button>
      </div>
    );
  }
}

export default Destroyer;