import {connect} from 'react-redux';
import SearchResults from './SearchResults';
import {getSearchCards} from '../../redux/cardsRedux';
import {getSearchColumns} from '../../redux/columnsRedux';
import {getSearchLists} from '../../redux/listsRedux';

const mapStateToProps = (state, props) => {
  const searchString = props.match.params.id;

  return {
    cards: getSearchCards(state, searchString),
    columns: getSearchColumns(state, searchString),
    lists: getSearchLists(state, searchString),
  };
};

export default connect(mapStateToProps)(SearchResults);