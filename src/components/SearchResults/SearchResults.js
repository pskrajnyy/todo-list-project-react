import React from 'react';
import styles from './SearchResults.scss';
import PropTypes from 'prop-types';
import Card from '../Card/CardContainer';
import {Link} from 'react-router-dom';
import ReactHtmlParser from 'react-html-parser';
import {searchResults} from '../../data/dataStore';

class SearchResults extends React.Component {
  static propTypes = {
    cards: PropTypes.array,
    columns: PropTypes.array,
    lists: PropTypes.array,
  };

  getCards(cards) {
    return (
      <div className={styles.cards}>
        <h2>CARDS</h2>
        {cards.length === 0 ? searchResults.cardsNotFoundDescription : cards.map(cardData => (
          <div key={cardData.id} className={styles.wrapper}>
            <Card key={cardData.id} {...cardData} />
            <Link className={styles.link} to={`/list/${cardData.listId}`}>
              <p>{ReactHtmlParser(`Column: ${cardData.columnName}, List: ${cardData.listName}`)}</p>
            </Link>
          </div>
        ))}
      </div>
    );
  }

  getColumns(columns) {
    return (
      <div className={styles.cards}>
        <h2>COLUMNS</h2>
        {columns.length === 0 ? searchResults.columnsNotFoundDescription : columns.map(columnData => (
          <div key={columnData.id} className={styles.wrapper}>

            <Link className={styles.link} to={`/list/${columnData.listId}`}>
              <p>{ReactHtmlParser(`List: ${columnData.listName}`)}</p>
            </Link>
          </div>
        ))}
      </div>
    );
  }

  getLists(lists) {
    return (
      <div className={styles.cards}>
        <h2>LISTS</h2>
        {lists.length === 0 ? searchResults.listsNotFoundDescription : lists.map(listData => (
          <div key={listData.id} className={styles.wrapper}>

            <Link className={styles.link} to={`/list/${listData.id}`}>
              <p>{ReactHtmlParser(`${listData.title}`)}</p>
            </Link>
          </div>
        ))}
      </div>
    );
  }

  render() {
    const {cards, lists, columns} = this.props;

    return (
      <section className={[styles.component, styles.notFound].join(' ')}>
        {this.getCards(cards)}
        {this.getColumns(columns)}
        {this.getLists(lists)}
      </section>
    );
  }
}

export default SearchResults;