import React from 'react';
import styles from './Header.scss';
import {NavLink, Link} from 'react-router-dom';
import Container from '../Container/Container';
import Icon from '../Icon/Icon';
import {header} from '../../data/dataStore';
import Search from '../Search/Search';

class Header extends React.Component {
  render() {
    return (
      <header className={styles.component}>
        <Container>
          <div className={styles.wrapper}>
            <div className={styles.searchWithIcon}>
              <Link to='/' className={styles.logo} >
                <Icon name='tasks' />
              </Link>
              <Search />
            </div>

            <nav>
              <NavLink exact to='/' activeClassName='active'>{header.home}</NavLink>
              <NavLink exact to='/info' activeClassName='active'>{header.info}</NavLink>
              <NavLink exact to='/FAQ' activeClassName='active'>{header.faq}</NavLink>
            </nav>
          </div>
        </Container>
      </header>
    );
  }
}

export default Header;