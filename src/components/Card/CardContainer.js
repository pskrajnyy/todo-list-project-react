import {connect} from 'react-redux';
import Card from './Card';
import {createActionRemoveCard} from '../../redux/cardsRedux';
const mapStateToProps = () => ({

});

const mapDispatchToProps = (dispatch, props) => ({
  removeCard: () => dispatch(createActionRemoveCard({
    id: props.id,
  })),
});

export default connect(mapStateToProps, mapDispatchToProps)(Card);