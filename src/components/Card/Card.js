import React from 'react';
import styles from './Card.scss';
import PropTypes from 'prop-types';
import {Draggable} from 'react-beautiful-dnd';
import Destroyer from '../Destroyer/Destroyer';

class Card extends React.Component {

  static propTypes = {
    title: PropTypes.string.isRequired,
    id: PropTypes.string,
    index: PropTypes.number,
    removeCard: PropTypes.func,
    site: PropTypes.string,
  };

  getHtml(title, removeCard) {
    return (
      <div className={styles.cardWithButton}>
        {title}
        <Destroyer action={() => removeCard()}/>
      </div>
    );
  }

  render() {
    const {title, id, index, removeCard} = this.props;
    if (window.location.href.includes('/search/')) {
      return (
        <article className={styles.component}>
          {this.getHtml(title, removeCard)}
        </article>
      );
    } else {
      return (
        <Draggable draggableId={id} index={index}>
          {provided => (
            <article
              className={styles.component}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
              ref={provided.innerRef}
            >
              {this.getHtml(title, removeCard)}
            </article>
          )}
        </Draggable>
      );
    }
  }
}

export default Card;