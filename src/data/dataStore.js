export const settings = {
  columnCreatorText: 'Add new column',
  listCreatorText: 'Add new list',
  cardCreatorText: 'Add new card',
  creator: {
    buttonOK: 'OK',
    buttonCancel: 'Cancel',
    defaultText: 'Add new item',
  },
  search: {
    defaultText: 'Search...',
    icon: 'search',
  },
  defaultListDescription: '<p>I can do all the things!!!</p>',
  defaultListImage: 'https://images.pexels.com/photos/313782/pexels-photo-313782.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
  defaultColumnIcon: 'list-alt',
};

export const pageContents = {
  title: 'My \'To do\' Application',
  subtitle: 'A simple to-do app, with lists, columns and cards',
};

export const listData = {
  title: 'Things to do <sup>soon!</sup>',
  description: 'Interesting things I want to check out!',
  image: 'http://uploads.kodilla.com/bootcamp/fer/11.react/space.png',
  columns: [
    {
      key: 0,
      title: 'Books',
      icon: 'book',
      cards: [
        {
          key: 0,
          title: 'This Is Going to Hurt',
        },
        {
          key: 1,
          title: 'Interpreter of Maladies',
        },
      ],
    },
    {
      key: 1,
      title: 'Movies',
      icon: 'film',
      cards: [
        {
          key: 0,
          title: 'Harry Potter',
        },
        {
          key: 1,
          title: 'Star Wars',
        },
      ],
    },
    {
      key: 2,
      title: 'Games',
      icon: 'gamepad',
      cards: [
        {
          key: 0,
          title: 'The Witcher',
        },
        {
          key: 1,
          title: 'Skyrim',
        },
      ],
    },
  ],
};

const lists = [
  {
    id: 'list-1',
    title: 'Movies to watch <sup>soon!</sup>',
    description: 'Interesting movies that I would like to watch!',
    image: 'https://images.pexels.com/photos/4057890/pexels-photo-4057890.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
  },
  {
    id: 'list-2',
    title: 'Books to read',
    description: 'Interesting books!',
    image: 'https://images.pexels.com/photos/159711/books-bookstore-book-reading-159711.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
  },
  {
    id: 'list-3',
    title: 'Tasks in Home',
    description: 'Household duties',
    image: 'https://images.pexels.com/photos/731082/pexels-photo-731082.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
  },
];

const columns = [
  {
    id: 'column-1',
    listId: 'list-1',
    title: 'To watch',
    icon: 'times-circle',
  },
  {
    id: 'column-2',
    listId: 'list-1',
    title: 'While watching',
    icon: 'spinner',
  },
  {
    id: 'column-3',
    listId: 'list-1',
    title: 'Viewed',
    icon: 'check',
  },
  {
    id: 'column-4',
    listId: 'list-2',
    title: 'Horror books',
    icon: 'book',
  },
  {
    id: 'column-5',
    listId: 'list-2',
    title: 'Fantasy books',
    icon: 'book',
  },
  {
    id: 'column-6',
    listId: 'list-2',
    title: 'Educational books',
    icon: 'book',
  },
  {
    id: 'column-7',
    listId: 'list-3',
    title: 'For me',
    icon: 'male',
  },
  {
    id: 'column-8',
    listId: 'list-3',
    title: 'For wife',
    icon: 'female',
  },
  {
    id: 'column-9',
    listId: 'list-3',
    title: 'For children',
    icon: 'child',
  },
];

const cards = [
  {
    id: 'card-1',
    index: 0,
    columnId: 'column-1',
    title: 'This Is Going to Hurt',
  },
  {
    id: 'card-2',
    index: 1,
    columnId: 'column-1',
    title: 'Interpreter of Maladies',
  },
  {
    id: 'card-3',
    index: 2,
    columnId: 'column-1',
    title: 'Play It as It Lays by Joan Didion',
  },
  {
    id: 'card-4',
    index: 3,
    columnId: 'column-1',
    title: 'Skippy Dies by Paul Murray',
  },
  {
    id: 'card-5',
    index: 0,
    columnId: 'column-2',
    title: 'Harry Potter',
  },
  {
    id: 'card-6',
    index: 1,
    columnId: 'column-2',
    title: 'Star Wars',
  },
  {
    id: 'card-7',
    index: 0,
    columnId: 'column-3',
    title: 'The Witcher',
  },
  {
    id: 'card-8',
    index: 1,
    columnId: 'column-3',
    title: 'Skyrim',
  },
  {
    id: 'card-9',
    index: 0,
    columnId: 'column-4',
    title: 'Frankenstein',
  },
  {
    id: 'card-10',
    index: 0,
    columnId: 'column-6',
    title: 'React 16 - Framework for professionals',
  },
  {
    id: 'card-11',
    index: 0,
    columnId: 'column-7',
    title: 'Pay bills',
  },
  {
    id: 'card-12',
    index: 1,
    columnId: 'column-7',
    title: 'Fix the tap',
  },
  {
    id: 'card-13',
    index: 0,
    columnId: 'column-8',
    title: 'Do shopping',
  },
  {
    id: 'card-14',
    index: 0,
    columnId: 'column-8',
    title: 'Clean room',
  },
  {
    id: 'card-15',
    index: 1,
    columnId: 'column-9',
    title: 'To go for a walk with a dog',
  },
];

export const infoContent = {
  title: 'Information',
  image: 'https://www.humanium.org/en/wp-content/uploads/2019/09/pic2-1024x665.jpg',
  subtitle: 'Info',
  description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
};

export const faqContent = {
  title: 'FAQ',
  image: 'https://www.enexus.pl/public/assets/icon/25-of-the-Best-Examples-of-Effective-FAQ-Pages.png',
};

export const faqQuestions = [
  {
    subtitle: 'Question 1',
    description: 'Ecce, assimilatio! Detrius noster abaculus est. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  },
  {
    subtitle: 'Question 2',
    description: 'Ecce, assimilatio! Detrius noster abaculus est. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  },
  {
    subtitle: 'Question 3',
    description: 'Ecce, assimilatio! Detrius noster abaculus est. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  },
];

export const header = {
  home: 'Home',
  info: 'Info',
  faq: 'FAQ',
};

export const searchResults = {
  cardsNotFoundDescription: 'The card you requested not found',
  columnsNotFoundDescription: 'The column you requested not found',
  listsNotFoundDescription: 'The list you requested not found',
};

const initialStoreData = {
  app: {...pageContents},
  lists: [...lists],
  columns: [...columns],
  cards: [...cards],
  info: {...infoContent},
  faq: {...faqContent},
  faqQuestions: [...faqQuestions],
  header: {...header},
  searchResults: {...searchResults},
};

export default initialStoreData;
