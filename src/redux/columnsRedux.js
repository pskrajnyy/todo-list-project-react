import shortid from 'shortid';

export const getColumnsForList = ({columns}, listId) => columns.filter(column => column.listId === listId);
export const getSearchColumns = ({ columns, lists }, searchString) => columns.filter(column => new RegExp(searchString, 'i').test(column.title)).map(column => ({
  ...column,
  listName: lists.find(list => list.id === column.listId).title,
}));
const reducerName = 'columns';
const createActionName = name => `app/${reducerName}/${name}`;

export const ADD_COLUMN = createActionName('ADD_COLUMN');

export const createActionAddColumn = payload => ({payload: {...payload, id: shortid.generate()}, type: ADD_COLUMN});

export default function reducer(state = [], action = []) {
  switch (action.type) {
    case ADD_COLUMN: {
      return [...state, action.payload];
    }
    default:
      return state;
  }
}