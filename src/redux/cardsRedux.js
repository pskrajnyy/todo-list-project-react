import shortid from 'shortid';

export const getCardsForColumn = ({cards}, columnId) => cards.filter(card => card.columnId == columnId).sort((a, b) => a.index - b.index);
export const getSearchCards = ({cards, columns, lists} , searchString) => cards.filter(card => new RegExp(searchString, 'i').test(card.title)).map(card => ({
  ...card,
  columnName: columns.find(column => column.id === card.columnId).title,
  listName: lists.find(list => list.id === columns.find(column => column.id === card.columnId).listId).title,
  listId: lists.find(list => list.id === columns.find(column => column.id === card.columnId).listId).id}));
const reducerName = 'cards';
const createActionName = name => `app/${reducerName}/${name}`;

export const ADD_CARD = createActionName('ADD_CARD');
export const MOVE_CARD = createActionName('MOVE_CARD');
export const REMOVE_CARD = createActionName('REMOVE_CARD');

export const createActionAddCard = payload => ({payload: {...payload, id: shortid.generate()}, type: ADD_CARD});
export const createActon_moveCard = payload => ({payload, type: MOVE_CARD});
export const createActionRemoveCard = payload => ({payload: {...payload}, type: REMOVE_CARD});

export default function reducer(state = [], action = []) {
  switch (action.type) {
    case ADD_CARD: {
      const data = {...action.payload};
      const cardsIdsInColumn = state.filter(card => (card.columnId === action.payload.columnId)).map(card => card.index);
      data.index = 0;
      if(cardsIdsInColumn.length > 0) {
        data.index = Math.max(...cardsIdsInColumn) + 1;
      }
      return [...state, data];
    }
    case MOVE_CARD: {
      const {id, src, dest} = action.payload;
      const targetCard = state.filter(card => card.id === id)[0];
      const targetColumnCards = state.filter(card => card.columnId === dest.columnId).sort((a, b) => a.index - b.index);

      if(dest.columnId === src.columnId) {
        targetColumnCards.splice(src.index, 1);
        targetColumnCards.splice(dest.index, 0, targetCard);

        return state.map(card => {
          const targetColumnIndex = targetColumnCards.indexOf(card);

          if(targetColumnIndex > -1 && card.index !== targetColumnIndex){
            return {...card, index: targetColumnIndex};
          } else {
            return card;
          }
        });
      } else {
        const sourceColumnCards = state.filter(card => card.columnId === src.columnId).sort((a, b) => a.index + b.index);
        sourceColumnCards.splice(src.index, 1);
        targetColumnCards.splice(dest.index, 0, targetCard);

        return state.map(card => {
          const targetColumnIndex = targetColumnCards.indexOf(card);

          if(card === targetCard){
            return {...card, index: targetColumnIndex, columnId: dest.columnId};
          } else if(targetColumnIndex > -1 && card.index !== targetColumnIndex){
            return {...card, index: targetColumnIndex};
          } else {
            const sourceColumnIndex = sourceColumnCards.indexOf(card);

            if(sourceColumnIndex > -1 && card.index !== sourceColumnIndex){
              return {...card, index: sourceColumnIndex};
            } else {
              return card;
            }
          }
        });
      }
    }

    case REMOVE_CARD: {
      return state.filter(card => card.id !== action.payload.id);
    }

    default:
      return state;
  }
}